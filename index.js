const { createFile } = require("./helpers/multiplicar.js");
const argv = require("./config/yargs.js");
require("colors");

console.clear();

createFile(argv.b, argv.t, argv.l)
  .then((fileName) => console.log(`${fileName} creado`.rainbow))
  .catch((e) => console.log(e));
