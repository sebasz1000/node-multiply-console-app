const argv = require("yargs")
  .option({
    b: {
      alias: "base",
      type: "number",
      demandOption: true,
      describe: "Es el factor a multiplicar",
    },
    l: {
      alias: "listar",
      demandOption: false,
      type: "boolean",
      describe: "MUestra el listado en consola",
    },
    t: {
      alias: "till",
      demandOption: false,
      default: "10",
      type: "number",
      describe: "Determines multiplying number limit",
    },
  })
  .check((argv, opts) => {
    if (isNaN(argv.b)) {
      throw new Error("La base tiene que ser un número");
    } else {
      return argv.b;
    }
  }).argv;

module.exports = argv;
