const fs = require("fs");
require("colors");

const createFile = async (multiplier = 5, till = 10, list = false) => {
  try {
    let output,
      consola = "";
    const fileName = `tabla-${multiplier}.txt`;

    for (let i = 1; i <= till; i++) {
      output += `${multiplier} x ${i} = ${multiplier * i} \n`;
      consola += `${multiplier.toString().yellow} ${"x".blue} ${
        i.toString().red
      } = ${multiplier * i} \n`;
    }

    list && console.log(consola);

    fs.writeFileSync(`./output/${fileName}`, output);

    return fileName;
  } catch (e) {
    throw e;
  }
};

module.exports = { createFile };
